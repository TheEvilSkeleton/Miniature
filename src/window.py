# window.py
#
# Copyright 2023 Miniature Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-or-later


from gi.repository import Adw, Gdk, Gio, GLib, Gtk
from PIL import Image, ImageChops, ImageOps

from . import supported_image_types
from .file_chooser import FileChooser


@Gtk.Template(resource_path="/io/gitlab/gregorni/Miniature/gtk/window.ui")
class MiniatureWindow(Adw.ApplicationWindow):
    __gtype_name__ = "MiniatureWindow"

    menu_btn = Gtk.Template.Child()
    toast_overlay = Gtk.Template.Child()
    main_stack = Gtk.Template.Child()
    spinner = Gtk.Template.Child()
    img_display_pic = Gtk.Template.Child()
    orig_size_row = Gtk.Template.Child()
    width_entry = Gtk.Template.Child()
    height_entry = Gtk.Template.Child()
    percent_spin = Gtk.Template.Child()
    save_btn = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        target = Gtk.DropTarget(
            formats=Gdk.ContentFormats.new_for_gtype(Gio.File),
            actions=Gdk.DragAction.COPY,
        )

        target.connect("drop", self.__on_drop)
        target.connect("enter", self.__on_enter)
        target.connect("leave", self.__on_leave)
        self.add_controller(target)

        settings = Gio.Settings(schema_id="io.gitlab.gregorni.Miniature")
        settings.bind("width", self, "default-width", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("height", self, "default-height", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("is-maximized", self, "maximized", Gio.SettingsBindFlags.DEFAULT)

        self.width_buff = self.width_entry.get_buffer()
        self.height_buff = self.height_entry.get_buffer()

        self.percent_spin.connect("value-changed", self.__on_percent_changed)
        self.width_entry.connect("changed", self.__on_width_changed)
        self.height_entry.connect("changed", self.__on_height_changed)
        self.save_btn.connect("clicked", self.__save_to_file)

        self.previous_stack = "welcome"
        self.file = None

    def on_open_file(self):
        self.__show_spinner()
        FileChooser.open_file(self, self.previous_stack)

    def check_is_image(self, file):
        def __wrong_image_type():
            print(f"{file.get_path()} is not of a supported image type.")
            self.toast_overlay.add_toast(
                Adw.Toast.new(
                    # Translators: Do not translate "{basename}"
                    _('"{basename}" is not of a supported image type.').format(
                        basename=file.get_basename()
                    )
                )
            )
            self.main_stack.set_visible_child_name(self.previous_stack)

        try:
            if self.file and file:
                if not ImageChops.difference(
                    Image.open(self.file.get_path()).convert("RGB"),
                    Image.open(file.get_path()).convert("RGB"),
                ).getbbox():
                    self.main_stack.set_visible_child_name(self.previous_stack)
                    return

            self.__show_spinner()
            print(f"Input file: {file.get_path()}")

            img = Image.open(file.get_path())
            if img.format.lower() in supported_image_types.SUPPORTED_IMAGE_TYPES:
                self.file = file
                img = ImageOps.exif_transpose(img)
                self.__display_image(img)
            else:
                __wrong_image_type()
        except IOError:
            __wrong_image_type()

    def __display_image(self, img):
        self.main_stack.set_visible_child_name("display-page")
        self.orig_size_row.set_subtitle(f"{img.width} x {img.height}")
        self.width_buff.set_text(str(img.width), -1)
        self.height_buff.set_text(str(img.height), -1)
        self.percent_spin.set_value(100)
        self.img = img

    def __save_to_file(self, *args):
        FileChooser.save_file(
            self,
            self.img,
            f"{path.splitext(GLib.basename(self.file.get_path()))[0]}-{self.width_buff.get_text()}x{self.height_buff.get_text()}-downscaled.{self.file.get_path().split('.').pop().lower()}",
        )

    def __on_percent_changed(self, *args):
        pass

    def __on_width_changed(self, *args):
        pass

    def __on_height_changed(self, *args):
        pass

    def __show_spinner(self):
        self.main_stack.set_visible_child_name("spinner-page")
        self.spinner.start()

    def __on_drop(self, widget, file, *args):
        self.check_is_image(file)

    def __on_enter(self, *args):
        self.previous_stack = self.main_stack.get_visible_child_name()
        self.main_stack.set_visible_child_name("drop-page")
        return Gdk.DragAction.COPY

    def __on_leave(self, *args):
        self.main_stack.set_visible_child_name(self.previous_stack)
